﻿#include <stdio.h>
#include <windows.h>
#include <math.h>

int main()
{
	int x = 1, a = 3;
	double res;
	printf("-----------------While-----------------\n");
	while (x <= 9)
	{
		res = exp(-x) * sqrt(1 + x);
		printf("e^-%d * sqrt(1+%d) = %.5lf\n", x, x, res);
		x = x + a;
		Sleep(100);
	}
	printf("\n\n\n");
	printf("------------------For------------------\n");

	for (x=1, a = 3; x <= 9; x=x+a)
	{
		res = exp(-x) * sqrt(1 + x);
		printf("e^-%d * sqrt(1+%d) = %.5lf\n", x, x, res);
		Sleep(100);
	}
	double x1=2.5, res1, a1=0.5;
	printf("\n\n\n");
	printf("-----------------While1-----------------\n");
	while (x1 <= 5)
	{
		res1 = (x1 * x1 + 1) + pow(x1, 2. / 3);
		printf("e^-%.2lf * sqrt(1+%.2lf) = %.2lf\n", x1, x1, res1);
		x1 = x1 + a1;
		Sleep(100);
	}
	printf("\n\n\n");
	printf("------------------For1------------------\n");

	for (x1=2.5, a1=0.5 ; x1 <= 5; x1 = x1 + a1)
	{
		res1 = (x1 * x1 + 1) + pow(x1, 2. / 3);
		printf("e^-%.2lf * sqrt(1+%.2lf) = %.2lf\n", x1, x1, res1);
		Sleep(100);
	}	printf("\n\n\n");
	return 0;
}