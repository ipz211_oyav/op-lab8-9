﻿#include <stdio.h>
#include <windows.h>

int main()
{
	double b=1, res, res2 ;
	int a = 1;
	printf("-----------------While-----------------\n");
	while (a <= 11)
	{
		
		res = a * b;
		printf("%d * %.1f =%.1f\n", a, b, res);
			a++;
			b = b + 0.2;
			Sleep(100);
	}
	printf("------------------For------------------\n");

	for (a = 1, b = 1; a <= 11; a++, b=b+0.2)
	{
		res2 = a * b;
		printf("%d * %0.1f = %0.1f\n", a, b, res2);
		Sleep(100);
	}
	return 0;
}
